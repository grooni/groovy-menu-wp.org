=== Groovy Menu (free) ===
Contributors: Grooni
Tags: mega menu, megamenu, navigation, mobile menu, drop down, menu, responsive, responsive menu, sticky menu, vertical menu, horizontal menu, ajax cart
Requires at least: 4.9.7
Tested up to: 5.2
Requires PHP: 7.0
Stable tag: 1.0.1
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.en.html

Groovy menu is a modern customizable and flexible WordPress Mega Menu Plugin designed for creating mobile friendly menus with a lot of options.

== Description ==
Groovy Menu is a WordPress Mega Menu Plugin that will allows you easily add an awesome mega menu on your site. Is an easy to customize, just need to upload your logo and fit your own colors, fonts and sizes.

= Features =
* Mega Menu
* Vertical menu
* One-page menu
* Dropdown menu
* Submenu
* Toolbar menu
* Ajax Cart
* WooCommerce integration
* Responsive menu
* Fullwidth mega menu
* Mobile menu
* RTL Support
* WPML Ready
* Multi-level menu support
* Smooth scroll
* GDPR Compliance
* Preview mode
* 2 hover types
* 1 desktop logo + 1 mobile logo
* Automatic and manual integration
* No coding skills needed
* Easy to customize

= Premium Features =
* Mega menu
* Mega menu blocks - that allow adding rich content into the mega menu. Compatibility with Elementor, WPBakery (Visual
Composer), Beaver Builder, Gutenberg
* Many header layouts
* Online preset library with pre-made presets
* Vertical menu
* Icon menu
* Sticky menu
* Fixed menu
* Off-canvas navigation
* Hamburger menu
* Submenu
* Sidebar menu
* Custom menu badges (icon, text, image)
* User roles with user role plugins
* Set specific menu for the taxonomies
* Premium Support
* Theme Developers features

= Plugin compatibility =
WooCommerce, WPBakery, Elementor, SiteOrigin, Beaver Builder, WPML,
Gutenberg

== Installation ==
1. Upload \"groovy-menu.zip\" to the \"/wp-content/plugins/\" directory.
2. Activate the plugin through the \"Plugins\" menu in WordPress.
3. Enable automatic integration from the WordPress admin page > Groovy menu > Integration

== Frequently Asked Questions ==
= How to integrate? =
Groovy menu can be integrated both manually and automatically. The automatic integration is the easiest and in most
cases the working way to implement Groovy Menu on your website. The principle of autointegration is that the Groovy Menu
 plugin will be displayed immediately after the opening HTML tag “body”. [Read more](https://grooni.com/docs/groovy-menu/integration/automatic-integration/)

= How to upload logo? =
Please read [this](https://grooni.com/docs/groovy-menu/global-settings-2/logo-settings/) article.

== Additional Info ==
The source code of the plugin can be found at [Bitbucket](https://bitbucket.org/grooni/groovy-menu-wp.org/src/master/)

== Screenshots ==
1. Classic dropdown menu.
2. Dashboard.
3. Global settings.
4. Preset options.
5. Mega menu with menu blocks.

== Changelog ==
= 1.0.1 =
* Initial release.
